import { NavLink } from "react-router-dom"
import s from '../../styles/mainStyle/main.module.scss'
import React from "react"

const Header = () => {
    return (
        <header>
            <div className={s.header__panel}>
                <NavLink to='/posts/page/1'>HomePage</NavLink>
            </div>
        </header>
    )
}

export default Header