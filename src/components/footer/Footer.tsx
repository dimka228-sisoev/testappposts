import React from 'react'
import s from '../../styles/mainStyle/main.module.scss'

const Footer = () => {
    return (
        <footer className={s.footer__panel}>
            <div>Sysoev D.A. <a href="https://gitlab.com/dimka228-sisoev">GitHub</a></div>
        </footer>
    )
}

export default Footer