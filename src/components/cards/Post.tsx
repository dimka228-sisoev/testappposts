import { useNavigate } from 'react-router-dom'
import s from '../../styles/cardStyle/card.module.scss'
import { useEffect, useState } from 'react';
import { BASE_URL } from "../../utils/path";
import b from '../../styles/buttonStyle/button.module.scss'
import React from 'react';
import { PostItem } from '../../pages/mainPage/MainPage';

const Post: React.FC<{ postData: PostItem }> = ({ postData }) => {
    const navigate = useNavigate();

    const [userName, setUserName] = useState<string>('');

    useEffect(() => {
        const getUserById = async () => {
            return await fetch(`${BASE_URL}/users/${postData.userId}`).then((res) => res.json());
        }

        getUserById().then((res) => setUserName(res.username));
    }, [postData])

    return (
        <div className={s.card}>
            <div>
                <h3>{postData.title}</h3>
            </div>
            <div>
                {postData.body}
            </div>
            <div className={s.card__button}>
                Author: {userName}
                <button className={b.button} onClick={() => navigate(`/post/${postData.id}`)}>
                    Открыть
                </button>
            </div>
        </div>
    )
}

export default Post