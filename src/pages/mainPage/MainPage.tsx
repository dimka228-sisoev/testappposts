import Post from "../../components/cards/Post"
import s from "../../styles/mainStyle/main.module.scss"
import { useEffect, useState } from "react"
import { BASE_URL } from "../../utils/path"
import { useParams } from "react-router-dom"
import { useNavigate } from 'react-router-dom'
import React from "react"
import { useInView } from 'react-intersection-observer';

export type PostItem = {
    userId: number;
    id: number;
    title: string;
    body: string;
}

export const initializePostValue = {
    userId: 0,
    id: 0,
    title: '',
    body: ''
};

const MainPage = () => {
    const navigate = useNavigate();

    const { page: currentPage } = useParams()

    const { ref, inView } = useInView({
        threshold: 1,
    });

    const [pageCount, setPageCount] = useState<number>(currentPage ? +currentPage - 1 : 0);
    const [posts, setPosts] = useState<[PostItem]>([initializePostValue]);

    const showNextPageButton = pageCount >=6 && pageCount < 10;

    const plusPageCount = (): void => {
        setPageCount(pageCount + 1);
        navigate(`/posts/page/${pageCount + 1}`);
    }

    useEffect(() => {
        if (pageCount < 6 && inView) {
            setTimeout(plusPageCount, 100);
        }
    }, [inView])

    useEffect(() => {
        const getPostsFetch = async (): Promise<[PostItem]> => {
            return await fetch(`${BASE_URL}/posts?_page${pageCount}&_limit=${pageCount * 10}`).then((res) => res.json());
        }

        getPostsFetch().then((res) => setPosts(res));
    }, [pageCount])

    return (
        <div className={s.container}>
            <div className={s.cards__box}>
                {posts.map((postItem: PostItem) => {
                    return (
                        <div key={postItem.id} className={s.cards__wrapper}>
                            <Post postData={postItem} />
                        </div>
                    )
                })}
                <div ref={ref} />
                {showNextPageButton && <button onClick={plusPageCount}>Показать ещё</button>}
            </div>
        </div>
    )
}

export default MainPage