import s from '../../styles/cardStyle/card.module.scss'
import { useParams } from "react-router-dom"
import { useState, useEffect } from 'react'
import { BASE_URL } from '../../utils/path'
import React from 'react'
import { PostItem, initializePostValue } from '../../pages/mainPage/MainPage';
import { useNavigate } from 'react-router-dom'

export type UserItem = {
    id: number;
    name: string;
    username: string;
    email: string;
    address: {
       street: string;
       suite: string;
       city: string;
       zipcode: string;
       geo: {
       lat: string;
       lng: string;
       };
    };
    phone: string;
    website: string;
    company: {
        name: string;
        catchPhrase: string;
        bs: string;
    };
}

const PostInfo = () => {
    const { id: postId } = useParams<string>()

    const navigate = useNavigate();

    if (Number.isInteger(+postId)) {
        navigate(`/posts/page/0`);
    }

    const [postData, setPostData] = useState<PostItem>(initializePostValue);
    const [userName, setUserName] = useState<string>('');

    useEffect(() => {
        const getPostById = async (): Promise<PostItem> => {
            return await fetch(`${BASE_URL}/posts/${postId}`).then((res) => res.json());
        }

        getPostById().then((res) => setPostData(res));
    }, [postId])

    useEffect(() => {
        const getUserNameById = async (): Promise<UserItem> => {
            return await fetch(`${BASE_URL}/users/${postData.userId}`).then((res) => res.json());
        }

        getUserNameById().then((res) => setUserName(res.username));
    }, [postData])

    return (
        <div className={s.card}>
            <div>
                <h3>{postData.title}</h3>
            </div>
            <div>
                {postData.body}
            </div>
            <div className={s.card__button}>
                Author: {userName}
            </div>
        </div>
    )
}

export default PostInfo