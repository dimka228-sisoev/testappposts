import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Footer from './components/footer/Footer';
import Header from './components/header/Header';
import MainPage from './pages/mainPage/MainPage';
import UserInfo from './pages/postinfo/PostInfo';

import s from './styles/mainStyle/main.module.scss'

function App() {

  return (
    <BrowserRouter>
      <div className={s.wrapper}>
        <Header />
        <main>
          <Routes>
            <Route path="*" element={<Navigate to="/posts/page/1" />} />
            <Route path="/posts/page/:page" element={<MainPage />} />
            <Route path="/post/:id" element={<UserInfo />} />
          </Routes>
        </main>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
